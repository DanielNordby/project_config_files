## dotfile configuration setup

This repository is an opinionated group of settings that is generic enough to be applied to any project. It includes things like linting rules, style enforcement, and IDE settings, along with any applicable npm packages necessary to make it opperate.

If your IDE supports it, it is recommended that you:

<ul>
<li>Enable Prettier in your IDE extensions
<ul>
<li>Set Prettier as default formatter</li>
<li>Enable auto-formatting on save</li>
</ul>
</li>
<li>Enable ESLint in your IDE extensions</li>
</ul>
